package rainy.file.synchronization.server.commond;

public interface CommondExecutor<T> {
	String supportedCommond();

	void executeCommand(Command<T> command);
}
